package com.mdsws.z80.prsr;

public class CdSntxExc extends RuntimeException {
	private static final long serialVersionUID = 7677102246828314753L;

	public CdSntxExc() {
		super();
	}

	public CdSntxExc(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CdSntxExc(String arg0) {
		super(arg0);
	}

	public CdSntxExc(Throwable arg0) {
		super(arg0);
	}

}
