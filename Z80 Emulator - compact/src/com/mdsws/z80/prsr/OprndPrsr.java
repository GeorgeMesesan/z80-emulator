package com.mdsws.z80.prsr;

import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Rgst;
import com.mdsws.z80.oprnd.BLOprnd;
import com.mdsws.z80.oprnd.DMAOprnd;
import com.mdsws.z80.oprnd.IMAOprnd;
import com.mdsws.z80.oprnd.ROprnd;

public class OprndPrsr {
	private static final String MA_START = "(";

	public Oprnd prs(String txt) {
		boolean memAcc=txt.startsWith(MA_START);
		if(memAcc)
		{
		 txt=txt.substring(1,txt.length()-1);
		}
		boolean rgst=isRgst(txt);

		if (rgst) 
		{
		if (memAcc) 
			{
		 return new IMAOprnd(txt);
		
		 }
		else
			{
			
			return new ROprnd(txt);
		  }
		} 
		else 
		{
		  if (memAcc)
			{
			 return new DMAOprnd((short)Integer.parseInt(txt));
		 }
		else
		 {
		return new BLOprnd((byte) Integer.parseInt(txt));
			}
		}
	}

	private boolean isRgst(String tx) {
		return tx.equals(Rgst.A) || tx.equals(Rgst.B) 
			||tx.equals(Rgst.C) ||tx.equals(Rgst.D)
		|| tx.equals(Rgst.E)||tx.equals(Rgst.F)||tx.equals(Rgst.H)				|| tx.equals(Rgst.L);
	}

}
