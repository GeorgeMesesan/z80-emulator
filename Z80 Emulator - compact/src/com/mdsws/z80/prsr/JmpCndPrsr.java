package com.mdsws.z80.prsr;

import com.mdsws.z80.instr.Jmp.Cnd;

public enum JmpCndPrsr {
	ZR("Z",Cnd.ZR),NZR("NZ",Cnd.NZR),PSTV("P",Cnd.PSTV),NGTV("N",Cnd.NGTV);

	private String sntx;
	private Cnd cnd;

	private JmpCndPrsr(String sntx, Cnd cnd) {
		this.sntx=sntx;
		this.cnd=cnd;
	}

	public String getSntx() {
		return sntx;
	}

	public Cnd getCnd() {
		return cnd;
	}

}
