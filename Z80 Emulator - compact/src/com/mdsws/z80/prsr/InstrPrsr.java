package com.mdsws.z80.prsr;

import com.mdsws.z80.Instr;
import com.mdsws.z80.Oprnd;
import com.mdsws.z80.instr.Adcrr;
import com.mdsws.z80.instr.Add;
import com.mdsws.z80.instr.Cmpr;
import com.mdsws.z80.instr.Dcrmt;
import com.mdsws.z80.instr.End;
import com.mdsws.z80.instr.Incrmt;
import com.mdsws.z80.instr.Jmp;
import com.mdsws.z80.instr.Ld;
import com.mdsws.z80.instr.Sbtr;
import com.mdsws.z80.instr.Jmp.Cnd;

public enum InstrPrsr {
	ADD("ADD") {
		@Override
		protected Instr prs(String[]tks) {
			if (tks.length!=2) {
			 throw new CdSntxExc("ADD must have one operand");
			}	
			return new Add(prsOprnd(tks[1]));
		}
	},
	ADCRR("ADC") {
		@Override
		protected Instr prs(String[]tks) {
			if (tks.length!=2) {
				throw new CdSntxExc("ADC must have one operand");
		}return new Adcrr(prsOprnd(tks[1]));
		}
	},
	SBTR("SUB") {
		@Override
		protected Instr prs(String[] tks) {
			if (tks.length!=2) {
			 throw new CdSntxExc("SUB must have one operand");
			}
			return new Sbtr(prsOprnd(tks[1]));
		}
	},
	CMPR("CP") {
		@Override
		protected Instr prs(String[] tks) {
			if (tks.length!=2)
			 throw new CdSntxExc("CP must have one operand");
			 return new Cmpr(prsOprnd(tks[1]));
		}
	},

	INCRMT("INC") {
		@Override
		protected Instr prs(String[]tks) {
			if (tks.length!= 2)
			 throw new CdSntxExc("INC must have one operand");
				return new Incrmt(prsOprnd(tks[1]));
		}
	},

	
	
	DCRMT("DEC") {
		@Override
		protected Instr prs(String[] tks) {
			if (tks.length !=2)
			throw new CdSntxExc("DEC must have one operand");
				return new Dcrmt(prsOprnd(tks[1]));
		}
	},
	LD("LD") {
		@Override
		protected Instr prs(String[]tks) {
			if (tks.length != 3)
				throw new CdSntxExc("LD must have two operands");
			return new Ld(prsOprnd(tks[1]),prsOprnd(tks[2]));
		}
	},
	JMP("JP") {
		@Override
		protected Instr prs(String[] tks){
			if (tks.length!= 2&& tks.length !=3)
			{
			 throw new CdSntxExc("JP must have one or two operands");
			}
			int adrs;
			Cnd cnd=Cnd.UNCND;
			if(tks.length==2) {
				adrs=Integer.parseInt(tks[1]);
			}else{
			  adrs= Integer.parseInt(tks[2]);
			  for (JmpCndPrsr jmpCndPrsr:JmpCndPrsr.values()){
					if (jmpCndPrsr.getSntx().equals(tks[1])){
							cnd=jmpCndPrsr.getCnd();
						break;
				}
				}
			}
			return new Jmp(cnd, adrs);
		}
	},

	
	
	END("END") {
		
		@Override
		
		protected Instr prs(String[]tks) {
			
			
			if (tks.length!= 1)
				
				
				throw new CdSntxExc("END doesn't have an operand");
			
			
			return new End();
			
		}
		
	};

	private String sntx;

	private InstrPrsr(String sntx) {
		
		this.sntx=sntx;
		
	}
	
	public String getSntx() {
		return sntx;
	}
	protected abstract Instr prs(String[]tks);
	protected Oprnd prsOprnd(String txt) {
		return new OprndPrsr().prs(txt);
	}
}
