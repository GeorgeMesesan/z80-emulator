package com.mdsws.z80;


public abstract class Oprnd {

	public abstract byte get(Prc prc);

	public abstract void set(Prc prc, byte b);
}
