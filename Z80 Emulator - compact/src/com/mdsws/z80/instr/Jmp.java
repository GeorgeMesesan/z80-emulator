package com.mdsws.z80.instr;

import com.mdsws.z80.Instr;
import com.mdsws.z80.Prc;

public class Jmp extends Instr {

	public static enum Cnd {
		UNCND,ZR,NZR,PSTV,NGTV
	}

	private Cnd cnd;
	private int adrs;

	public Jmp(Cnd cnd,int adrs) {
		this.cnd=cnd;
		this.adrs=adrs;
	}

	@Override
	public void exc(Prc prc) {
		if (isJmp(prc))
		{
			prc.setPC(adrs);
		}
		else
		{
			prc.incPC();
		}
	}

	private boolean isJmp(Prc prc) {
		if (cnd == Cnd.ZR 
				&& !prc.getFlgsRgst().getZr())
		{
			return false;
		}
		if (cnd == Cnd.NZR 
				&& prc.getFlgsRgst().getZr())
		{
			return false;
		}
		if (cnd == Cnd.PSTV 
				&& prc.getFlgsRgst().getSgn())
		{
			return false;
		}
		if (cnd == Cnd.NGTV 
				&& !prc.getFlgsRgst().getSgn())
		{
			return false;
		}
		return true;
	}
}
