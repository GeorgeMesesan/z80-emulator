package com.mdsws.z80.instr;

import com.mdsws.z80.Instr;
import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Prc;
import com.mdsws.z80.Rgst;

public class Cmpr extends Instr {
	private Oprnd oprnd																;

	public Cmpr		(Oprnd 				oprnd) {
		this.oprnd 	= oprnd															;
	}

	@Override
	public void exc(Prc 				prc) {
		Rgst acc 	= prc				.getAccRgst()								;
		int r 		= acc				.get() 				- oprnd		.get(prc)	;
		prc			.getFlgsRgst()		.setFlags(r)								;
		prc			.incPC()														;
	}

}
