package com.mdsws.z80.instr;

import com.mdsws.z80.Instr;
import com.mdsws.z80.Prc;

public class End extends Instr {

	@Override
	public void exc(Prc prc) {
		
		prc . end();
		
	}

}
