package com.mdsws.z80.instr;

import com.mdsws.z80.Instr;
import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Prc;
import com.mdsws.z80.Rgst;

public class Adcrr extends Instr {
	private Oprnd oprnd;

	public Adcrr(Oprnd oprnd) {
		this.oprnd = oprnd;
	}

	@Override
	public void exc(Prc prc) {
		Rgst acc=prc.getAccRgst();
		int r=addUnsgnd(acc.get(),oprnd.get(prc))
				+prc.getFlgsRgst().getCrryVal();
		prc.getFlgsRgst().setFlags(r);
		acc.set((byte)r);
		prc.incPC();
	}

	private int addUnsgnd(byte x1,byte x2) {
		return(0x00ff&x1)+(0x00ff&x2);
	}

}
