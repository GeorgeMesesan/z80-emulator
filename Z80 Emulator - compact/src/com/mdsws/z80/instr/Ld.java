package com.mdsws.z80.instr;



import com.mdsws.z80.Instr;

import com.mdsws.z80.Oprnd;

import com.mdsws.z80.Prc;



public class Ld extends Instr{
	
	
	private Oprnd dst;
	
	
	private Oprnd src;
	
	
	

	public Ld(Oprnd dst, Oprnd src)
	
	{
		
		
		this.dst=dst;
		
		
		
		this.src=src;
		
		
	}

	@Override
	public void exc(Prc prc)
	
	{
		
		
		dst.set(prc,src.get(prc));
		
		
		prc.incPC();
		
		
	}
}
