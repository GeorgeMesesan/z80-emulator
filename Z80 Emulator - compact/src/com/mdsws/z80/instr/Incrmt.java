package com.mdsws.z80.instr;
import com.mdsws.z80.Instr;
import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Prc;
public class Incrmt extends Instr {
	private Oprnd oprnd;
	public Incrmt(Oprnd oprnd) {
		this.oprnd = oprnd;
	}
	@Override
	public void exc(Prc prc) {
		int r=oprnd.get(prc)+1;
		prc.getFlgsRgst().setFlags(r);
		oprnd.set(prc,(byte)r);
		prc.incPC();
	}
}
