package com.mdsws.z80;

public class Flgs extends Rgst {
	private static final int CRRY = 0;
	private static final int ZR = 6;
	private static final int SGN = 7;

	public Flgs(String n) {
		super(n);
	}

	public void setFlags(int rslt) {
		setCrry(rslt<0||rslt> 255);
		setZr((0x00ff&rslt) == 0);
		setSgn((0x00ff&rslt)<0||(0x00ff&rslt)>Byte.MAX_VALUE);
	}

	public boolean getCrry() {
		return get(CRRY);
	}

	public void setCrry(boolean b) {
		set(CRRY,b);
	}

	public int getCrryVal() {
		return getCrry()?1:0;
	}

	public boolean getZr() {
		return get(ZR);
	}

	public void setZr(boolean b) {
		set(ZR, b);
	}

	public boolean getSgn() {
		return get(SGN);
	}

	public void setSgn(boolean b) {
		set(SGN, b);
	}

	private boolean get(int b) {
		return ((x>>b)&1)==1;
	}

	private void set(int bt,boolean b) {
		int m=1<<bt;
		if (b)
		{
			this.x|=m;
		}
		else
		{
			this.x&=~m;
		}
	}

}
