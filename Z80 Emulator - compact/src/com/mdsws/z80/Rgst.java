package com.mdsws.z80;

public class Rgst {
	public static final String A = "A";
	public static final String B = "B";
	public static final String C = "C";
	public static final String D = "D";
	public static final String E = "E";
	public static final String F = "F";
	public static final String H = "H";
	public static final String L = "L";

	private String nm;
	protected byte x;
	
	public Rgst(String n) {
		this.nm = n;
	}
	
	public String getNm() {
		return nm;
	}

	public byte get() {
		return x;
	}

	public void set(byte x) {
		this.x = x;
	}
}
