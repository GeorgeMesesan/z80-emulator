package com.mdsws.z80;

public class Mem {
	private static final int MXS = 1 << 16;

	private byte[] bs=new byte[MXS];

	public byte get(short adrs) {
		return bs[adrs];
	}

	public void set(short adrs, byte b) {
		bs[adrs]=b;
	}

	public short getW(short adrs) {
		byte hb=get(adrs);
		byte lb=get((short)(adrs + 1));
		return (short)(hb<<8+ lb);
	}

	public void setW(short adrs, short s) {
		set (  			adrs,  		(byte) (s >> 8) ); // high byte first
		set	(	(short)(adrs + 1),	(byte)		s	); // low byte second
	}
}
