package com.mdsws.z80;

import java.util.HashMap;
import java.util.Map;

public class Prc {
	private Map<String, Rgst> rgsts=new HashMap<String, Rgst>();

	private Mem mem=new Mem();
	private int pc;
	private boolean end;

	public Prc() {
		addRgst	(	new Rgst	(	Rgst.A	)	)	;
		addRgst	(	new Rgst	(	Rgst.B	)	)	;
		addRgst	(	new Rgst	(	Rgst.C	)	)	;
		addRgst	(	new Rgst	(	Rgst.D	)	)	;
		addRgst	(	new Rgst	(	Rgst.E	)	)	;
		addRgst	(	new Flgs	(	Rgst.F	)	)	;
		addRgst	(	new Rgst	(	Rgst.H	)	)	;
		addRgst	(	new Rgst	(	Rgst.L	)	)	;
	}

	private void addRgst(Rgst rgst) {
		  rgsts.put(rgst.getNm(),rgst);
	}

	public Rgst getRgst(String nm) {
	  return rgsts.get(nm);
	}

	public Rgst getAccRgst() {
		return rgsts.get(Rgst.A);
	}

	public Flgs getFlgsRgst() {
			return (Flgs)rgsts.get(Rgst.F);
	}

	public Mem getMem() {
		return mem;
	}

	public int getPC() {
		return pc;
	}

	public void setPC(int pc) {
	 this.pc=pc;
	}
	
	public void incPC() {
	 this.pc++;
	}

	public void end() {
	 end=true;
	}

	public void excInstr(Instr[] instr) {
			end=false;
			pc=0;
			do  {
			 instr[pc].exc(this);
				} 
			while 
			 (end!=false);
	}
}
