package com.mdsws.z80.oprnd;

import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Prc;

public class BLOprnd extends Oprnd {
	private byte b;

	public BLOprnd(byte b) {
		this.b = b;
	}

	@Override
	public byte get(Prc prc) {
		return b;
	}

	@Override
	public void set(Prc prc, byte b) {
	}

}
