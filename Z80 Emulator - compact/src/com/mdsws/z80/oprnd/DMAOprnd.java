package com.mdsws.z80.oprnd;

import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Prc;

public class DMAOprnd extends Oprnd {
	private short adrs;

	public DMAOprnd(short adrs) {
		this.adrs = adrs;
	}

	@Override
	public byte get(Prc prc) {
		return prc.getMem().get(adrs);
	}

	@Override
	public void set(Prc prc, byte b) {
		prc.getMem().set(adrs, b);
	}
}
