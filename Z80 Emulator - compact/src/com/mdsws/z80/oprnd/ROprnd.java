package com.mdsws.z80.oprnd;

import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Prc;

public class ROprnd extends Oprnd {
	private String rNm;

	public ROprnd(String rNm) {
		this.rNm = rNm;
	}

	@Override
	public byte get(Prc prc) {
		return prc.getRgst(rNm).get();
	}

	@Override
	public void set(Prc prc, byte b) {
		prc.getRgst(rNm).set(b);
	}

}
