package com.mdsws.z80.oprnd;

import com.mdsws.z80.Oprnd;
import com.mdsws.z80.Prc;

public class IMAOprnd extends Oprnd {
	private String rNm;

	public IMAOprnd(String rNm) {
		this.rNm = rNm;
	}

	@Override
	public byte get(Prc prc) {
		short adrs = prc.getRgst(rNm).get();
		return prc.getMem().get(adrs);
	}

	@Override
	public void set(Prc prc, byte b) {
		short adrs = prc.getRgst(rNm).get();
		prc.getMem().set(adrs, b);
	}

}
