package com.mdsws.z80;

public class FlagsRegister extends Register {
	private static final int CARRY_FLAG_BIT = 0;
	private static final int ZERO_FLAG_BIT = 6;
	private static final int SIGN_FLAG_BIT = 7;

	public FlagsRegister(String name) {
		super(name);
	}

	public void setFlags(int operationResult) {
		setCarryFlag(operationResult < 0 || operationResult > 255);
		setZeroFlag((0x00ff & operationResult) == 0);
		setSignFlag((0x00ff & operationResult) < 0 || (0x00ff & operationResult) > Byte.MAX_VALUE);
	}

	public boolean getCarryFlag() {
		return getBitValue(CARRY_FLAG_BIT);
	}

	public void setCarryFlag(boolean value) {
		setBitValue(CARRY_FLAG_BIT, value);
	}

	public int getCarryValue() {
		return getCarryFlag() ? 1 : 0;
	}

	public boolean getZeroFlag() {
		return getBitValue(ZERO_FLAG_BIT);
	}

	public void setZeroFlag(boolean value) {
		setBitValue(ZERO_FLAG_BIT, value);
	}

	public boolean getSignFlag() {
		return getBitValue(SIGN_FLAG_BIT);
	}

	public void setSignFlag(boolean value) {
		setBitValue(SIGN_FLAG_BIT, value);
	}

	private boolean getBitValue(int bit) {
		return ((value >> bit) & 1) == 1;
	}

	private void setBitValue(int bit, boolean value) {
		int mask = 1 << bit;
		if (value)
			this.value |= mask;
		else
			this.value &= ~mask;
	}

}
