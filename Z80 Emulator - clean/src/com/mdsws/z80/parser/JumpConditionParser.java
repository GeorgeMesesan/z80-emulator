package com.mdsws.z80.parser;

import com.mdsws.z80.instructions.Jump.Condition;

public enum JumpConditionParser {
	ZERO("Z", Condition.ZERO), NOT_ZERO("NZ", Condition.NOT_ZERO), POSITIVE("P", Condition.POSITIVE), NEGATIVE("N",
			Condition.NEGATIVE);

	private String syntax;
	private Condition condition;

	private JumpConditionParser(String syntax, Condition condition) {
		this.syntax = syntax;
		this.condition = condition;
	}

	public String getSyntax() {
		return syntax;
	}

	public Condition getCondition() {
		return condition;
	}

}
