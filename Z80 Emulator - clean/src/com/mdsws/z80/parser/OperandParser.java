package com.mdsws.z80.parser;

import com.mdsws.z80.Operand;
import com.mdsws.z80.Register;
import com.mdsws.z80.operands.ByteLiteralOperand;
import com.mdsws.z80.operands.DirectMemoryAccessOperand;
import com.mdsws.z80.operands.IndirectMemoryAccessOperand;
import com.mdsws.z80.operands.RegisterOperand;

public class OperandParser {
	private static final String MEMORY_ACCESS_INDICATOR = "(";

	public Operand parseOperand(String text) {
		boolean isMemoryAccess = text.startsWith(MEMORY_ACCESS_INDICATOR);
		if (isMemoryAccess)
			text = text.substring(1, text.length() - 1);
		boolean isRegisterName = isRegisterName(text);

		if (isRegisterName) {
			if (isMemoryAccess)
				return new IndirectMemoryAccessOperand(text);
			else
				return new RegisterOperand(text);
		} else {
			if (isMemoryAccess)
				return new DirectMemoryAccessOperand((short) Integer.parseInt(text));
			else
				return new ByteLiteralOperand((byte) Integer.parseInt(text));
		}
	}

	private boolean isRegisterName(String text) {
		return text.equals(Register.A) || text.equals(Register.B) || text.equals(Register.C) || text.equals(Register.D)
				|| text.equals(Register.E) || text.equals(Register.F) || text.equals(Register.H)
				|| text.equals(Register.L);
	}

}
