package com.mdsws.z80.parser;

public class CodeSyntaxException extends RuntimeException {
	private static final long serialVersionUID = 7677102246828314753L;

	public CodeSyntaxException() {
		super();
	}

	public CodeSyntaxException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CodeSyntaxException(String arg0) {
		super(arg0);
	}

	public CodeSyntaxException(Throwable arg0) {
		super(arg0);
	}

}
