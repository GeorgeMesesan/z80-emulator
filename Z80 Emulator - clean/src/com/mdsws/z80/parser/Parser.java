package com.mdsws.z80.parser;

import java.util.StringTokenizer;

import com.mdsws.z80.Instruction;

public class Parser {

	public Instruction[] parse(String[] lines) {
		Instruction[] instructions = new Instruction[lines.length];
		for (int i = 0; i < lines.length; i++)
			instructions[i] = parseInstruction(lines[i]);
		return instructions;
	}

	private Instruction parseInstruction(String text) {
		String[] tokens = split(text, " ,");
		for (InstructionParser instructionParser : InstructionParser.values()) {
			if (instructionParser.getSyntax().equals(tokens[0]))
				return instructionParser.parse(tokens);
		}

		throw new CodeSyntaxException("unknown instruction " + text);
	}

	private String[] split(String text, String separators) {
		StringTokenizer tokenizer = new StringTokenizer(text, separators);
		String[] tokens = new String[tokenizer.countTokens()];
		int i = 0;
		while (tokenizer.hasMoreTokens())
			tokens[++i] = tokenizer.nextToken();
		return tokens;
	}

}
