package com.mdsws.z80.parser;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Operand;
import com.mdsws.z80.instructions.Add;
import com.mdsws.z80.instructions.AddWithCarry;
import com.mdsws.z80.instructions.Compare;
import com.mdsws.z80.instructions.Decrement;
import com.mdsws.z80.instructions.End;
import com.mdsws.z80.instructions.Increment;
import com.mdsws.z80.instructions.Jump;
import com.mdsws.z80.instructions.Load;
import com.mdsws.z80.instructions.Subtract;
import com.mdsws.z80.instructions.Jump.Condition;

public enum InstructionParser {
	ADD("ADD") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 2)
				throw new CodeSyntaxException("ADD must have one operand");
			return new Add(parseOperand(tokens[1]));
		}
	},

	ADD_WITH_CARRY("ADC") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 2)
				throw new CodeSyntaxException("ADC must have one operand");
			return new AddWithCarry(parseOperand(tokens[1]));
		}
	},

	SUBTRACT("SUB") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 2)
				throw new CodeSyntaxException("SUB must have one operand");
			return new Subtract(parseOperand(tokens[1]));
		}
	},

	COMPARE("CP") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 2)
				throw new CodeSyntaxException("CP must have one operand");
			return new Compare(parseOperand(tokens[1]));
		}
	},

	INCREMENT("INC") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 2)
				throw new CodeSyntaxException("INC must have one operand");
			return new Increment(parseOperand(tokens[1]));
		}
	},

	DECREMENT("DEC") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 2)
				throw new CodeSyntaxException("DEC must have one operand");
			return new Decrement(parseOperand(tokens[1]));
		}
	},

	LOAD("LD") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 3)
				throw new CodeSyntaxException("LD must have two operands");
			return new Load(parseOperand(tokens[1]), parseOperand(tokens[2]));
		}
	},

	JUMP("JP") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 2 && tokens.length != 3)
				throw new CodeSyntaxException("JP must have one or two operands");

			int address;
			Condition condition = Condition.UNCONDITIONAL;
			if (tokens.length == 2) {
				address = Integer.parseInt(tokens[1]);
			} else {
				address = Integer.parseInt(tokens[2]);
				for (JumpConditionParser conditionParser : JumpConditionParser.values()) {
					if (conditionParser.getSyntax().equals(tokens[1])) {
						condition = conditionParser.getCondition();
						break;
					}
				}
			}
			return new Jump(condition, address);
		}
	},

	END("END") {
		@Override
		protected Instruction parse(String[] tokens) {
			if (tokens.length != 1)
				throw new CodeSyntaxException("END doesn't have an operand");
			return new End();
		}
	};

	private String syntax;

	private InstructionParser(String syntax) {
		this.syntax = syntax;
	}

	public String getSyntax() {
		return syntax;
	}

	protected abstract Instruction parse(String[] tokens);

	protected Operand parseOperand(String text) {
		return new OperandParser().parseOperand(text);
	}
}
