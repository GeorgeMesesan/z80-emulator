package com.mdsws.z80;

import java.util.HashMap;
import java.util.Map;

public class Processor {
	private Map<String, Register> registers = new HashMap<String, Register>();

	private Memory memory = new Memory();
	private int programCounter;
	private boolean ended;

	public Processor() {
		addRegister(new Register(Register.A));
		addRegister(new Register(Register.B));
		addRegister(new Register(Register.C));
		addRegister(new Register(Register.D));
		addRegister(new Register(Register.E));
		addRegister(new FlagsRegister(Register.F));
		addRegister(new Register(Register.H));
		addRegister(new Register(Register.L));
	}

	private void addRegister(Register register) {
		registers.put(register.getName(), register);
	}

	public Register getRegister(String name) {
		return registers.get(name);
	}

	public Register getAccumulatorRegister() {
		return registers.get(Register.A);
	}

	public FlagsRegister getFlagsRegister() {
		return (FlagsRegister) registers.get(Register.F);
	}

	public Memory getMemory() {
		return memory;
	}

	public int getProgramCounter() {
		return programCounter;
	}

	public void setProgramCounter(int programCounter) {
		this.programCounter = programCounter;
	}
	
	public void incrementProgramCounter() {
		this.programCounter++;
	}

	public void end() {
		ended = true;
	}

	public void executeInstructions(Instruction[] instructions) {
		ended = false;
		programCounter = 0;
		do {
			instructions[programCounter].executeOn(this);
		} while (ended != false);
	}
}
