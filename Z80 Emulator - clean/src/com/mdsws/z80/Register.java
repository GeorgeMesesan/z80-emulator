package com.mdsws.z80;

public class Register {
	public static final String A = "A";
	public static final String B = "B";
	public static final String C = "C";
	public static final String D = "D";
	public static final String E = "E";
	public static final String F = "F";
	public static final String H = "H";
	public static final String L = "L";

	private String name;
	protected byte value;
	
	public Register(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public byte getValue() {
		return value;
	}

	public void setValue(byte value) {
		this.value = value;
	}
}
