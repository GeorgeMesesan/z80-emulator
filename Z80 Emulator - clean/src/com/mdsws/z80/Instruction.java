package com.mdsws.z80;

public abstract class Instruction {
	
	public abstract void executeOn(Processor processor);
	
}
