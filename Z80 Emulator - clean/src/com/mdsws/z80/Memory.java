package com.mdsws.z80;

public class Memory {
	private static final int MAX_SIZE = 1 << 16;

	private byte[] bytes = new byte[MAX_SIZE];

	public byte getByteValue(short address) {
		return bytes[address];
	}

	public void setByteValue(short address, byte value) {
		bytes[address] = value;
	}

	public short getWordValue(short address) {
		byte highByte = getByteValue(address);
		byte lowByte = getByteValue((short) (address + 1));
		return (short) (highByte << 8 + lowByte);
	}

	public void setWordValue(short address, short value) {
		setByteValue(address, (byte) (value >> 8)); // high byte first
		setByteValue((short) (address + 1), (byte) value); // low byte second
	}
}
