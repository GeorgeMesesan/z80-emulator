package com.mdsws.z80.instructions;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;

public class Load extends Instruction {
	private Operand destination;
	private Operand source;

	public Load(Operand destination, Operand source) {
		this.destination = destination;
		this.source = source;
	}

	@Override
	public void executeOn(Processor processor) {
		destination.setValue(processor, source.getValue(processor));
		processor.incrementProgramCounter();
	}

}
