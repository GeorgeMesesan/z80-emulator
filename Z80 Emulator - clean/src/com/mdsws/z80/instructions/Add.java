package com.mdsws.z80.instructions;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;
import com.mdsws.z80.Register;

public class Add extends Instruction {
	private Operand operand;

	public Add(Operand operand) {
		this.operand = operand;
	}

	@Override
	public void executeOn(Processor processor) {
		Register accumulator = processor.getAccumulatorRegister();
		int result = addUnsigned(accumulator.getValue(), operand.getValue(processor));
		processor.getFlagsRegister().setFlags(result);
		accumulator.setValue((byte) result);
		processor.incrementProgramCounter();
	}

	private int addUnsigned(byte value1, byte value2) {
		return (0x00ff & value1) + (0x00ff & value2);
	}

}
