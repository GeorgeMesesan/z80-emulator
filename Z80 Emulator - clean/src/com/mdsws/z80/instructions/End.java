package com.mdsws.z80.instructions;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Processor;

public class End extends Instruction {

	@Override
	public void executeOn(Processor processor) {
		processor.end();
	}

}
