package com.mdsws.z80.instructions;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;
import com.mdsws.z80.Register;

public class Subtract extends Instruction {
	private Operand operand;

	public Subtract(Operand operand) {
		this.operand = operand;
	}

	@Override
	public void executeOn(Processor processor) {
		Register accumulator = processor.getAccumulatorRegister();
		int result = accumulator.getValue() - operand.getValue(processor);
		processor.getFlagsRegister().setFlags(result);
		accumulator.setValue((byte) result);
		processor.incrementProgramCounter();
	}

}
