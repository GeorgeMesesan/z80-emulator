package com.mdsws.z80.instructions;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;

public class Increment extends Instruction {
	private Operand operand;

	public Increment(Operand operand) {
		this.operand = operand;
	}

	@Override
	public void executeOn(Processor processor) {
		int result = operand.getValue(processor) + 1;
		processor.getFlagsRegister().setFlags(result);
		operand.setValue(processor, (byte) result);
		processor.incrementProgramCounter();
	}

}
