package com.mdsws.z80.instructions;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Processor;

public class Jump extends Instruction {

	public static enum Condition {
		UNCONDITIONAL, ZERO, NOT_ZERO, POSITIVE, NEGATIVE
	}

	private Condition condition;
	private int address;

	public Jump(Condition condition, int address) {
		this.condition = condition;
		this.address = address;
	}

	@Override
	public void executeOn(Processor processor) {
		if (areConditionsFulfilled(processor))
			processor.setProgramCounter(address);
		else
			processor.incrementProgramCounter();
	}

	private boolean areConditionsFulfilled(Processor processor) {
		if (condition == Condition.ZERO && !processor.getFlagsRegister().getZeroFlag())
			return false;
		if (condition == Condition.NOT_ZERO && processor.getFlagsRegister().getZeroFlag())
			return false;
		if (condition == Condition.POSITIVE && processor.getFlagsRegister().getSignFlag())
			return false;
		if (condition == Condition.NEGATIVE && !processor.getFlagsRegister().getSignFlag())
			return false;

		return true;
	}

}
