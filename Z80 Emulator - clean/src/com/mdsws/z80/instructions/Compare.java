package com.mdsws.z80.instructions;

import com.mdsws.z80.Instruction;
import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;
import com.mdsws.z80.Register;

public class Compare extends Instruction {
	private Operand operand;

	public Compare(Operand operand) {
		this.operand = operand;
	}

	@Override
	public void executeOn(Processor processor) {
		Register accumulator = processor.getAccumulatorRegister();
		int result = accumulator.getValue() - operand.getValue(processor);
		processor.getFlagsRegister().setFlags(result);
		processor.incrementProgramCounter();
	}

}
