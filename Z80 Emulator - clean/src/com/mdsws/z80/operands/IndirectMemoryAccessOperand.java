package com.mdsws.z80.operands;

import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;

public class IndirectMemoryAccessOperand extends Operand {
	private String registerName;

	public IndirectMemoryAccessOperand(String registerName) {
		this.registerName = registerName;
	}

	@Override
	public byte getValue(Processor processor) {
		short address = processor.getRegister(registerName).getValue();
		return processor.getMemory().getByteValue(address);
	}

	@Override
	public void setValue(Processor processor, byte value) {
		short address = processor.getRegister(registerName).getValue();
		processor.getMemory().setByteValue(address, value);
	}

}
