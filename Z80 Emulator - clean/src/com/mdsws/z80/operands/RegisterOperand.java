package com.mdsws.z80.operands;

import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;

public class RegisterOperand extends Operand {
	private String registerName;

	public RegisterOperand(String registerName) {
		this.registerName = registerName;
	}

	@Override
	public byte getValue(Processor processor) {
		return processor.getRegister(registerName).getValue();
	}

	@Override
	public void setValue(Processor processor, byte value) {
		processor.getRegister(registerName).setValue(value);
	}

}
