package com.mdsws.z80.operands;

import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;

public class DirectMemoryAccessOperand extends Operand {
	private short address;

	public DirectMemoryAccessOperand(short address) {
		this.address = address;
	}

	@Override
	public byte getValue(Processor processor) {
		return processor.getMemory().getByteValue(address);
	}

	@Override
	public void setValue(Processor processor, byte value) {
		processor.getMemory().setByteValue(address, value);
	}
}
