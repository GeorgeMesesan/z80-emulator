package com.mdsws.z80.operands;

import com.mdsws.z80.Operand;
import com.mdsws.z80.Processor;

public class ByteLiteralOperand extends Operand {
	private byte value;

	public ByteLiteralOperand(byte value) {
		this.value = value;
	}

	@Override
	public byte getValue(Processor processor) {
		return value;
	}

	@Override
	public void setValue(Processor processor, byte value) {
	}

}
