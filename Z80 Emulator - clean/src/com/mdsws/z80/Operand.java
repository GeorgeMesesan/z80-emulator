package com.mdsws.z80;

public abstract class Operand {

	public abstract byte getValue(Processor processor);

	public abstract void setValue(Processor processor, byte value);
}
