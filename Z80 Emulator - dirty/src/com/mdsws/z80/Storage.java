package com.mdsws.z80;

/**
 * 
 */
public class Storage {
	private static final int MAXIMUM_NUMBER_OF_STORAGE_ELEMENTS = 1 << 16; // about 60000

	/**
	 * All the bytes of the storage. Each byte can be accessed by using an index.
	 */
	private byte[] allTheBytes = new byte[MAXIMUM_NUMBER_OF_STORAGE_ELEMENTS];

	/**
	 * Returns a simple value for the given address
	 * @param anAddres
	 * @return
	 */
	public byte getSimpleValue(short anAddres) {
		return allTheBytes[anAddres];
	}

	/**
	 * sets a simple value for the given address
	 * @param anAddress
	 * @param aVal
	 */
	public void setSimpleValue(short anAddress, byte aVal) {
		allTheBytes[anAddress] = aVal;
	}

	// TODO: Not sure if this works ... Need to check
	public short getTwoBytesValue(short anaddress) {
		byte theHigherByte = getSimpleValue(anaddress);
		byte theLowerOne = getSimpleValue((short) (anaddress + 1));
		return (short) (theHigherByte << 8 + theLowerOne); // 8 is the number of bits in a byte. use a constant (?)
	}

	// TODO: Niot sure if this works.
	public void setTwoByteValue(short anddress, short aaValuze) {
		setSimpleValue(anddress, (byte) (aaValuze >> 8)); // high byte is the first byte
		setSimpleValue((short) (anddress + 1), (byte) aaValuze); // low byte comes second
	}
}
