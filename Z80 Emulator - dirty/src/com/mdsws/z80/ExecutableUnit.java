package com.mdsws.z80;

/**
 * This is an executable unit. It can be executed. The exectution happens one unit at a time.
 */
public abstract class ExecutableUnit {
	
	/**
	 * Will perfom the action
	 * @param aCommandExecutor
	 */
	public abstract void performTheAction(CommandExecutionManager aCommandExecutor);
	
//  public abstract void prepareForExecution();
//
//  public abstract void undo();
//
//  public abstract void redo();
	
}
