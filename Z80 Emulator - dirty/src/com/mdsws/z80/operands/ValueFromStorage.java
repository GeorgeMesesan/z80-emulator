package com.mdsws.z80.operands;

import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ExecutionElement;

/**
 *  a value for the storage
 *
 */
public class ValueFromStorage extends ExecutionElement {
	private short theStorageLocation;

	public ValueFromStorage(short aLocation) {
		this.theStorageLocation = aLocation;
	}

	@Override
	public byte getVaalue(CommandExecutionManager theExecutor) {
		return theExecutor.getTheStorage().getSimpleValue(theStorageLocation); // retzurns directly from storage
	}

	@Override
	public void setVAlue(CommandExecutionManager theExecutor, byte aValue) {
		theExecutor.getTheStorage().setSimpleValue(theStorageLocation, aValue); // puts the value directly into the storage
	}
}
