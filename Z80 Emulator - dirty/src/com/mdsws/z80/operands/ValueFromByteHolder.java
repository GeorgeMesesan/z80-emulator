package com.mdsws.z80.operands;

import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ExecutionElement;

/**
 * A value form the byte holder of the main component
 *
 */
public class ValueFromByteHolder extends ExecutionElement {
	private String theNameOfHolder;

	public ValueFromByteHolder(String aName) {
		this.theNameOfHolder = aName;
	}

	@Override
	public byte getVaalue(CommandExecutionManager theExecutor) {
	  return theExecutor.getByteHolder(theNameOfHolder).getValue();
	}

	@Override
	public void setVAlue(CommandExecutionManager theExecutor, byte value) {
			theExecutor.getByteHolder(theNameOfHolder).setValue(value);
	}

}
