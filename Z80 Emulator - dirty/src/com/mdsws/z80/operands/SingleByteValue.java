package com.mdsws.z80.operands;

import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ExecutionElement;

/**
 * A single byte value that can be querieed by the execution units
 *
 */
public class SingleByteValue extends ExecutionElement {
	private byte theValue;

	public SingleByteValue(byte aValue) {
		this.theValue = aValue;
	}

	@Override
	public byte getVaalue(CommandExecutionManager manager) {
		return theValue;
	}

	@Override
	public void setVAlue(CommandExecutionManager manager, byte aValue) {
	}

}
