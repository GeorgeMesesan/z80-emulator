package com.mdsws.z80.operands;

import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ExecutionElement;

/**
 * Concrete objects of this class (ValueFromStorageIndirectly) will put values into and returns values from the main system storage by using explicitly the actual name of a holder to pinpoint indirectly an exact location in the main storage at which the desired value was previously saved persistently and at which location will be persisted at a later moment by the actual callers of the appropriate methods.
 */
public class ValueFromStorageIndirectly extends ExecutionElement {
	private String nameOfHolder;

	public ValueFromStorageIndirectly(String aName) {
		this.nameOfHolder = aName;
	}

	@Override
	public byte getVaalue(CommandExecutionManager theEzecutor) {
	  short theLocation = theEzecutor.getByteHolder(nameOfHolder).getValue();
      return theEzecutor.getTheStorage().getSimpleValue(theLocation);
	}

	@Override
	public void setVAlue(CommandExecutionManager theExecutor, byte aValue) {
	 short theLocation=theExecutor.getByteHolder(nameOfHolder).getValue();
	 theExecutor.getTheStorage().setSimpleValue(theLocation,aValue);
	}

}
