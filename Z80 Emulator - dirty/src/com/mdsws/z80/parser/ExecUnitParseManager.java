package com.mdsws.z80.parser;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.ExecutionElement;
import com.mdsws.z80.intructions.AddingInstructin;
import com.mdsws.z80.intructions.AddingWithCarryInsruction;
import com.mdsws.z80.intructions.CompareingInstrction;
import com.mdsws.z80.intructions.DecrementingInstructon;
import com.mdsws.z80.intructions.EndingInstruction;
import com.mdsws.z80.intructions.IncrementingIntruction;
import com.mdsws.z80.intructions.JumpinInstrution;
import com.mdsws.z80.intructions.JumpinInstrution.PredicateDecision;
import com.mdsws.z80.intructions.LoadingInstruction;
import com.mdsws.z80.intructions.SubtractingInstruction;

/**
 * Parses an execution unit by investigatin the given strings
 * @author Administrator
 *
 */
public enum ExecUnitParseManager {
	ADDING("ADD") { // ADDING
		@Override
		protected ExecutableUnit handleParse(String[] theParseElements) {
			if (theParseElements.length!=2) {
				// in this case throw an exception
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("ADD must have one operand");
			}
			return new AddingInstructin(parseOneElement(theParseElements[1]));
		}
	},

	ADDING_WITH_CARRY_FLAG("ADC") { // ADDING_WITH_CARRY_FLAG
		@Override
		protected ExecutableUnit handleParse(String[] theParsElements) {
			if (theParsElements.length != 2) {
				// in this case throw an exception
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("ADC must have one operand");
			}
			return new AddingWithCarryInsruction(parseOneElement(theParsElements[1]));
		}
	},

	SUBTRACTING("SUB") { // SUBTRACTING
		@Override
		protected ExecutableUnit handleParse(String[] theParseElemnts) {
			if (theParseElemnts.length != 2)
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("SUB must have one operand");
			return new SubtractingInstruction(parseOneElement(theParseElemnts[1]));
		}
	},

	COMPARING("CP") { // COMPARING
		@Override
		protected ExecutableUnit handleParse(String[] someParseElements) {
			if (someParseElements.length != 2)
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("CP must have one operand");
			return new CompareingInstrction(parseOneElement(someParseElements[1]));
		}
	},

	INCREMENTING("INC") { // INCREMENTING
		@Override
		protected ExecutableUnit handleParse(String[] allParseElements) {
			if (allParseElements.length != 2)
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("INC must have one operand");
			return new IncrementingIntruction(parseOneElement(allParseElements[1]));
		}
	},

	DECREMENTING("DEC") { // DECREMENTING
		@Override
		protected ExecutableUnit handleParse(String[] remainingParseElements) {
			if (remainingParseElements.length != 2)
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("DEC must have one operand");
			return new DecrementingInstructon(parseOneElement(remainingParseElements[1]));
		}
	},

	LOADING("LD") { // LOADING
		@Override
		protected ExecutableUnit handleParse(String[] theParseElements) {
			if (theParseElements.length != 3)
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("LD must have two operands");
			return new LoadingInstruction(parseOneElement(theParseElements[1]), parseOneElement(theParseElements[2]));
		}
	},

	JUMPING("JP") { // JUMPING
		@Override
		protected ExecutableUnit handleParse(String[] theParseElement) {
			// final version
			if (theParseElement.length != 2 && theParseElement.length != 3)
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("JP must have one or two operands");

			// theParseElement[1] = theParseElement[0];
			// System.out.println("" + theParseElement.length);
			
			int programLocation;
			PredicateDecision condition = PredicateDecision.ALWAYS;
			if (theParseElement.length == 2) {
				// global
				programLocation = Integer.parseInt(theParseElement[1]);
			} else {
				// local
				programLocation = Integer.parseInt(theParseElement[2]);
				for (JumpDecisionParseManager conditionParser : JumpDecisionParseManager.values()) {
					if (conditionParser.getDecisionNaming().equals(theParseElement[1])) {
						condition = conditionParser.getCondition();
						break; // break the if condition
					}
				}
			}
			// returns an new JumpInstruction for the condition and the program location
			return new JumpinInstrution(condition, programLocation);
		}
	},

	ENDING("END") { // ENDING
		@Override
		protected ExecutableUnit handleParse(String[] theParseElements) {
			if (theParseElements.length != 1)
				throw new ProgramCodeSyntaxIsIncorrectRuntimeException("END doesn't have an operand");
			return new EndingInstruction();
		}
	};

	/**
	 * an attribute
	 */
	private String myNaming;

	/**
	 * The constructor of the class
	 * @param aNaming
	 */
	private ExecUnitParseManager(String aNaming) {
		this.myNaming = aNaming;
	}

	/**
	 * Returns the naming. Naming is here somehting like the syntax of the parser
	 * @return
	 */
	public String getNaming() {
		return myNaming;
	}

	/**
	 * Handles the parse command
	 * @param theParseElements
	 * @return
	 */
	protected abstract ExecutableUnit handleParse(String[] theParseElements);

	/**
	 * Parses exactly one element
	 * @param theText
	 * @return
	 */
	protected ExecutionElement parseOneElement(String theText) {
		return new ElementParseManager().handleParse(theText);
	}
}
