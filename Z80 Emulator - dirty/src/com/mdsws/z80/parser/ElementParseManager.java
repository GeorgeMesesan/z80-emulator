package com.mdsws.z80.parser;

import com.mdsws.z80.ByteValueHolder;
import com.mdsws.z80.ExecutionElement;
import com.mdsws.z80.operands.SingleByteValue;
import com.mdsws.z80.operands.ValueFromStorage;
import com.mdsws.z80.operands.ValueFromStorageIndirectly;
import com.mdsws.z80.operands.ValueFromByteHolder;

/**
 * The parser manager of an element. Will parse an element by investigating the given text
 * @author Administrator
 *
 */
public class ElementParseManager {
	private static final String MEMORY_ACCESS_INDICATOR_CHARACTER = "("; // the indicator (

	// handles the parse call
	// TODO: ugly - fix me as soon as possible (10.01.2011)
	public ExecutionElement handleParse(String theText) {
		boolean itIsMemoryAccess=theText.startsWith(MEMORY_ACCESS_INDICATOR_CHARACTER);
		if (itIsMemoryAccess) // if true
	     theText=theText.substring(1,theText.length()-1); // extract the string from 1 to almost the end
		boolean itIsAByteHolder=isAByteHolderName(theText); // ask if it is the byte holder (aka registar)

		// how should we deal with this?
		// Decision Mr.Ansprechpartner (01.03.2012): use a if-else-if construct
		if (itIsAByteHolder) { // if true
		  if (itIsMemoryAccess)
				return new ValueFromStorageIndirectly(theText); // if true
		else
		 return new ValueFromByteHolder(theText); // if fasle
			// end first if
		} else {
			if(itIsMemoryAccess)
		      return new ValueFromStorage((short)Integer.parseInt(theText));
			else
			 return new SingleByteValue((byte)Integer.parseInt(theText));
			// end first if
		} // end the big if
	}

	private boolean isAByteHolderName(String theText) {
		// check against all the byte holder values A to L
		return theText.equals(ByteValueHolder.HOLDER_1) 
				|| theText.equals(ByteValueHolder.HOLDER_2) 
			|| theText.equals(ByteValueHolder.HOLDER_3) || theText.equals(ByteValueHolder.HOLDER_4)
				|| theText.equals(ByteValueHolder.HOLDER_5) 
			
		|| theText.equals(ByteValueHolder.HOLDER_6) || theText.equals(ByteValueHolder.HOLDER_7)
				|| theText.equals(ByteValueHolder.HOLDER_8);
	}

}
