package com.mdsws.z80.parser;

import java.util.StringTokenizer;

import com.mdsws.z80.ExecutableUnit;

public class ParseManager {

	// FIXME: add some Unit testcases
	public ExecutableUnit[] doTheParse(String[] lines) {
		
		// creates an array of lines.length and of type ExecutableUnit. Saves this created array in a local variable called executableUnits of type ExecutableUnit[]
		// will throw an exception of lines.length is negative or greater than Integer.MAX_VALUE
		// could it throw a NullPointerException, should we check for this ?
		ExecutableUnit[] executableUnits = new ExecutableUnit[lines.length];
		
		// performs a for loop on the lines. Goes from 0 to lines.length
		// Decision 01.04.2012
		for (int y=0;y<lines.length;y++){executableUnits[y]=doIt(lines[y]);} // the end of the for loop
		
		// returns the result
		return executableUnits;
	}

	// FIXME: add some Unit testcases
	private ExecutableUnit doIt(String text) {
		
		// Changed for Bugfix 2012
		String[] allTokens = splitAllTokens(text, " ,");
		
		// Decision Mr.Ansprechpartner from (29.02.2012): don't unwind
		// unwindParameters();
		// extractAllNecessaryElements();
		// concatenateResourceMarkers();
		
		for (ExecUnitParseManager localElementParser : ExecUnitParseManager.values()) {
			
			
			if (localElementParser.getNaming().equals(allTokens[0])) {
				
				// try {
				return localElementParser.handleParse(allTokens);
				// } catch (NullPointerException e) {
				// 		// ignore -> how do we handle ?
				// }
				
			} // end of the if
			
		} // end of the for loop

		// throw an exception in case we arrive here -> should never happen !
		throw new ProgramCodeSyntaxIsIncorrectRuntimeException("unknown instruction " + text);
	}

	// FIXME: add some Unit testcases
	private String[] splitAllTokens(String theText, String allSeparators) {
		
		// mixed
		StringTokenizer theStringTokenizer = new StringTokenizer(theText, allSeparators);
		
		// according to the specification
		String[] allTheTokens = new String[theStringTokenizer.countTokens()];
		
		
		int y = 0; // fixed
		
		
		while (theStringTokenizer.hasMoreTokens())
			
			// TODO: check it!
			allTheTokens[++y] = theStringTokenizer.nextToken();
		
		return allTheTokens;
	}

}
