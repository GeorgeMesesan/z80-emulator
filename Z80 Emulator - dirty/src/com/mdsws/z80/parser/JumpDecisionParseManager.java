package com.mdsws.z80.parser;

import com.mdsws.z80.intructions.JumpinInstrution.PredicateDecision;

/**
 * The JumpDecisionParseManager 
 * @see PredicateDecision
 */
public enum JumpDecisionParseManager {
	ZERO("Z", PredicateDecision.IF_ZERO), NOT_ZERO("NZ", PredicateDecision.IF_NOT_ZERO), POSITIVE("P", PredicateDecision.IF_POSITIVE), NEGATIVE("N",
			PredicateDecision.IF_NOT_POSITIVE);

	private String theSyntax;
	private PredicateDecision condition;

	private JumpDecisionParseManager(String syntax, PredicateDecision condition) {
		this.theSyntax = syntax;
		this.condition = condition;
	}

	public String getDecisionNaming() {
		return theSyntax;
	}

	public PredicateDecision getCondition() {
		return condition;
	}

}
