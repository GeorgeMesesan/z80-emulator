package com.mdsws.z80.parser;

public class ProgramCodeSyntaxIsIncorrectRuntimeException extends RuntimeException {
private static final long serialVersionUID = 7677102246828314753L;
public ProgramCodeSyntaxIsIncorrectRuntimeException() {
super();
}
public ProgramCodeSyntaxIsIncorrectRuntimeException(String arg0, Throwable arg1) {super(arg0, arg1);}
public ProgramCodeSyntaxIsIncorrectRuntimeException(String arg0) {super(arg0);
}
public ProgramCodeSyntaxIsIncorrectRuntimeException(Throwable arg0) {
super(arg0);
}
}
