package com.mdsws.z80;

/**
 * This class represents an Execution element. It can be used in executions.
 */
public abstract class ExecutionElement {

	/**
	 * Return the value
	 * @param aCommandExecutor - the command executor
	 * @return - the byte value
	 */
	public abstract byte getVaalue(CommandExecutionManager aCommandExecutor);

	/**
	 * Sets the value
	 * @param aCommandExecutor - the command executor
	 * @param aByteValue - the byte value
	 */
	public abstract void setVAlue(CommandExecutionManager aCommandExecutor, byte aByteValue);
}
