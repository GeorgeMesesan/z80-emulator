package com.mdsws.z80;

import java.util.HashMap;
import java.util.Map;

/**
 * see first
 * {@linkplain ExecutableUnit}
 * and
 * {@linkplain ExecutionElement}
 */
public class CommandExecutionManager {
	// all the holders
	private Map<String, ByteValueHolder> theHolders = new HashMap<String, ByteValueHolder>();

	/**
	 * my storage attribute
	 */
	private Storage theStorage = new Storage();
	/**
	 * my execution position in the execution queue
	 */
	private int myExecutionPosition;
	/**
	 * the boolean to check
	 */
	private boolean itHasEndedForGood;

	/**
	 * The consturctor. Returns an object of type {@link CommandExecutionManager}
	 */
	public CommandExecutionManager() {
		// here we will add all holders to the map
		addANewHolderToTheMap(new ByteValueHolder(ByteValueHolder.HOLDER_1)); // adding the A
		addANewHolderToTheMap(new ByteValueHolder(ByteValueHolder.HOLDER_2)); // adding the B
		addANewHolderToTheMap(new ByteValueHolder(ByteValueHolder.HOLDER_3));// adding the C
		addANewHolderToTheMap(new ByteValueHolder(ByteValueHolder.HOLDER_4)); // adding the D
		addANewHolderToTheMap(new ByteValueHolder(ByteValueHolder.HOLDER_5)); // now the E
		addANewHolderToTheMap(new ByteValueHolderAsBits(ByteValueHolder.HOLDER_6)); // and the F
		addANewHolderToTheMap(new ByteValueHolder(ByteValueHolder.HOLDER_7)); // finally the H and the K
		addANewHolderToTheMap(new ByteValueHolder(ByteValueHolder.HOLDER_8));
	}

	/**
	 * adds a new holder to the big map of holders
	 * @param aValueHolder
	 */
	private void addANewHolderToTheMap(ByteValueHolder aValueHolder) {
		theHolders.put(aValueHolder.getItsName(),aValueHolder);
	}

	/**
	 * the byte holder
	 * @param aName
	 * @return
	 */
	public ByteValueHolder getByteHolder(String aName) {
		return theHolders.get(aName);
	}

	/**
	 * get the byte value holder known as the accumulator (a.k.a Accumulator)
	 * @return
	 */
	public ByteValueHolder getTheByteValueHolderActingAsAccumulator() {
		return theHolders.get(ByteValueHolder.HOLDER_1);
	}

	/**
	 * returns all the bits flags as one object
	 * @return
	 */
	public ByteValueHolderAsBits getAllTheBitsFlags() {
		return (ByteValueHolderAsBits)theHolders.get(ByteValueHolder.HOLDER_6);
	}

	/**
	 * Returns the main storage
	 * @return
	 */
	public Storage getTheStorage() {
		return theStorage;
	}

	
//	public void _testExecuteInstructions(Instruction[] instructions) {
//		ended = false;
//		programCounter = 0;
//		do {
//			instructions[programCounter].executeOn(this);
//		} while (!ended);
//	}
	
	/**
	 * This methods will return an integer representing the execution position that the manager is currently holding
	 * @return
	 */
	public int getTheExecutionPosition() {
		return myExecutionPosition;
	}

	/**
	 * Sets the value
	 * @param aNewExecutionPosition
	 */
	public void setTheExecutionPosition(int aNewExecutionPosition) {
		this.myExecutionPosition=aNewExecutionPosition;
	}
	
	/**
	 * Adds one to it
	 */
	public void addOneToTheExecutionPositionValue() {
		this.myExecutionPosition++; // the same as myExecutionPosition = myExecutionPosition + 1;
	}

	/**
	 * This method should only be called if the called is sure that the execution should end for good
	 * It will irrevocably set the itHasEndedForGood flag to true, meaning that the value will be true, in other words,
	 * the the execution will be stopped for good.
	 * This method is potentially thread unsafe.
	 */
	public void endForGood() {
		itHasEndedForGood=true; // reset
	}

	// self explanatory
	public void executeCompletely(ExecutableUnit[]allTheUnits) {
		// changed for request 1147
		itHasEndedForGood=false; // setting to true
		// changed for request 1147
		myExecutionPosition= 0; // setting the value to 1
		do {
		 // @ABA: commented here for testing purposes - don't forget to bring back for production
		 // prepareExecution(allTheUnits[myExecutionPosition]);
		 // prepareExceptionHandlers();
		 // initializeLocalStorage();
		 // prepareExecutionStack();
			allTheUnits[myExecutionPosition].performTheAction(this);
		 // cleanUp();
		 // TODO: add logging
		} while (itHasEndedForGood!=false); // is it OK (?)
	}
}
