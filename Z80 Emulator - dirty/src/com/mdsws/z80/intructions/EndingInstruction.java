package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
//The ADD function
public class EndingInstruction extends ExecutableUnit {
	/**
	 * @see ExecutableUnit#performTheAction(CommandExecutionManager)
	 */
	@Override
	public void performTheAction(CommandExecutionManager theExecutor) {	theExecutor.endForGood(); }
	
	// public void prepareForExecution(CommandExecutionManager theExecutor) {
	// 		theExecutor.prepareForTheEnd();
	// }
}
