package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.parser.JumpDecisionParseManager;
//The ADD function
public class JumpinInstrution extends ExecutableUnit {

	/**
	 * The predicate decision enumeration
	 * @see JumpDecisionParseManager
	 */
	public static enum PredicateDecision {
		ALWAYS, IF_ZERO, IF_NOT_ZERO, IF_POSITIVE, IF_NOT_POSITIVE
	}

	// changed for request 1147
	private PredicateDecision decision;
	private int location;

	/**
	 * The constructor
	 * @param anElement
	 */
	public JumpinInstrution(PredicateDecision decition, int aLocation) {
		this.decision = decition;
		this.location = aLocation;
	}

	/**
	 * @see ExecutableUnit#performTheAction(CommandExecutionManager)
	 */
	@Override
	public void performTheAction(CommandExecutionManager theExecutorManager) {
		if (isPredicateValueTrue(theExecutorManager)) 
		{
			// changed for request 1145
			theExecutorManager.setTheExecutionPosition(location);
		}
		else
		{
			theExecutorManager.addOneToTheExecutionPositionValue();
			// changed for request 1147
		}
	}

	private boolean isPredicateValueTrue(CommandExecutionManager theExecutor) {
		// shouldn't we use a switch here (?)
		if (decision == PredicateDecision.IF_ZERO 
				&& !theExecutor.getAllTheBitsFlags().getZeroBit())
		{
			return false; // really ?
		}
		if (decision == PredicateDecision.IF_NOT_ZERO 
				&& theExecutor.getAllTheBitsFlags().getZeroBit())
		{
			return false;
			// return true;
		}
		if (decision == PredicateDecision.IF_POSITIVE
				&& theExecutor.getAllTheBitsFlags().getSignFlag()) 
		{
			// return true;
			return false;
		}
		if (decision == PredicateDecision.IF_NOT_POSITIVE 
				&& !theExecutor.getAllTheBitsFlags().getSignFlag())
		{
			// return Boolean.FALSE.booleanValue(); <- shouldn't we use more objects ? (OOP !)
			return false;
		}

		return true; // default
	}

}
