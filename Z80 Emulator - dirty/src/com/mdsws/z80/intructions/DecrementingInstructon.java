package com.mdsws.z80.intructions;
import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ExecutionElement;
//The ADD function
public class DecrementingInstructon extends ExecutableUnit {
	private ExecutionElement local;
	/**
	 * The constructor
	 * @param anElement
	 */
	public DecrementingInstructon(ExecutionElement singleElement) {
		this.local=singleElement;
	}
	/**
	 * @see ExecutableUnit#performTheAction(CommandExecutionManager)
	 */
	@Override
	public void performTheAction(CommandExecutionManager theProcessor) {
		int theResult=local.getVaalue(theProcessor)+1;
		theProcessor.getAllTheBitsFlags().setAllFlagValues(theResult);
		local.setVAlue(theProcessor,(byte)theResult);
		theProcessor.addOneToTheExecutionPositionValue();
	}
}
