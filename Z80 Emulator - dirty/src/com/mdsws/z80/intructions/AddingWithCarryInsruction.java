package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ByteValueHolder;
import com.mdsws.z80.ExecutionElement;

//The so-called ADD with carry function
public class AddingWithCarryInsruction extends ExecutableUnit {
	
	
	
	private ExecutionElement theElement;
	public AddingWithCarryInsruction(ExecutionElement theOperand) {
		// setting the attribute
		this.theElement =  theOperand;
	}

	@Override
	public void performTheAction(CommandExecutionManager  theExecutionManager) {
		// getting the accumulator
		ByteValueHolder accumulator = theExecutionManager.getTheByteValueHolderActingAsAccumulator();
		
		// calling doIt
		int theResult=doIt(accumulator.getValue(), theElement.getVaalue( theExecutionManager))
				+ theExecutionManager.getAllTheBitsFlags().getTheCarryFlagBitAsIntegerValue();
		
		// calling all flag flag values
		theExecutionManager.getAllTheBitsFlags().setAllFlagValues(theResult );
		
		// setting the value
		accumulator.setValue( (byte) theResult );
		
		// adding one to the execution position
		theExecutionManager.addOneToTheExecutionPositionValue( );
		
	}
	
	/**
	 * actually doing it
	 * @param value1
	 * @param value2
	 * @return a value that is a function of the two arguments
	 */
	private int doIt(byte value1, byte value2) {
		// performing the operation on the arguments
		return ( ( 0x00ff /* the mask */) & ( value1 ) ) + ( ( 0x00ff /* the same mask */) & ( value2 ) );
	}

}
