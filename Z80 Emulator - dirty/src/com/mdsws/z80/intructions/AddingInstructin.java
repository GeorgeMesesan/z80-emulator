package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ByteValueHolder;
import com.mdsws.z80.ExecutionElement;

// The ADD function
public class AddingInstructin extends ExecutableUnit {
	private ExecutionElement singleElement;

	/**
	 * The constructor
	 * @param anElement
	 */
	public AddingInstructin(ExecutionElement snglElement) {
		this.singleElement=snglElement;
	}

	/**
	 * This method performs the simple operation of adding one value to the other
	 */
	@Override
	public void performTheAction(CommandExecutionManager theExecutor) {
		// 1.getting the accumulator
		ByteValueHolder accumulator=theExecutor.getTheByteValueHolderActingAsAccumulator();
		// 2.computing the result
		int theResult=doIt(accumulator.getValue(),singleElement.getVaalue(theExecutor));
		// 3.getting first and then setting all the flag values
		theExecutor.getAllTheBitsFlags().setAllFlagValues(theResult);
		// 3. setting the value
		accumulator.setValue((byte)theResult);
		// 5. adding one to the execution position value
		theExecutor.addOneToTheExecutionPositionValue();
	}

	/**
	 * actually doing it
	 * @param value1
	 * @param value2
	 * @return
	 */
	private int doIt(byte value1,byte value2) {
		// perform the operation ! make it so ! engage ! ;-)
		return(0x00ff&value1)+(0x00ff&value2);
	}

}
