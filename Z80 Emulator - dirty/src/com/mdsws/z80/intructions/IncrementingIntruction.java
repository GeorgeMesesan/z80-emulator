package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ExecutionElement;
//The ADD function
public class IncrementingIntruction extends ExecutableUnit {
	private ExecutionElement singleElement;

	/**
	 * The constructor
	 * @param anElement
	 */
	public IncrementingIntruction(ExecutionElement anElement) {
	this.singleElement = anElement;
	}

	/**
	 * @see ExecutableUnit#performTheAction(CommandExecutionManager)
	 */
	@Override
	public void performTheAction(CommandExecutionManager theExecutorManager) {
		int res = singleElement.getVaalue(theExecutorManager) + 1;
		// res += theExecutorManager.getValue(ByteValueHolder.B);
		// res += theExecutorManager.getValue(ByteValueHolder.C);
		theExecutorManager.getAllTheBitsFlags().setAllFlagValues(res);
		// theExecutorManager.getAllTheBitsFlags().setAllFlagValues(res);
		singleElement.setVAlue(theExecutorManager, (byte) res);
		theExecutorManager.addOneToTheExecutionPositionValue(); // will add a one (1)
	}

}
