package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ByteValueHolder;
import com.mdsws.z80.ExecutionElement;
//The ADD function
public class SubtractingInstruction extends ExecutableUnit {
	private ExecutionElement singleElement;

	/**
	 * The constructor
	 * @param anElement
	 */
	public SubtractingInstruction(ExecutionElement anElement) {
		this.singleElement = anElement;
	}

	/**
	 * @see ExecutableUnit#performTheAction(CommandExecutionManager)
	 */
	@Override
	public void performTheAction(CommandExecutionManager theExecutor) {
		ByteValueHolder theAccumulator = theExecutor.getTheByteValueHolderActingAsAccumulator();
		int theResult = theAccumulator.getValue() - singleElement.getVaalue(theExecutor); // remove it
		
		
		theExecutor.getAllTheBitsFlags().setAllFlagValues(theResult);
		theAccumulator.setValue((byte) theResult); // might trow a ClassCastException ?
		
		// try {
		theExecutor.addOneToTheExecutionPositionValue();
		// } catch (NullPointerException e) {
		// 		// ignore
		// }
	}

}
