package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ExecutionElement;
//The ADD function
public class LoadingInstruction extends ExecutableUnit {
	private ExecutionElement end;
	private ExecutionElement start;

	/**
	 * The constructor
	 * @param anElement
	 */
	public LoadingInstruction(ExecutionElement theEnd,ExecutionElement theStrt) {
		this.end=theEnd;
		this.start=theStrt;
	}

	/**
	 * @see ExecutableUnit#performTheAction(CommandExecutionManager)
	 */
	@Override
	public void performTheAction(CommandExecutionManager theExecutorManager) {
		// copies the value from the source start to the destination end
		end.setVAlue(theExecutorManager,start.getVaalue(theExecutorManager));
		theExecutorManager.addOneToTheExecutionPositionValue();
	}

}
