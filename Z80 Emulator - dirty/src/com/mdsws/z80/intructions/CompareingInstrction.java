package com.mdsws.z80.intructions;

import com.mdsws.z80.ExecutableUnit;
import com.mdsws.z80.CommandExecutionManager;
import com.mdsws.z80.ByteValueHolder;
import com.mdsws.z80.ExecutionElement;

//The ADD function
public class CompareingInstrction extends ExecutableUnit {
	
	
	private ExecutionElement localElement;
	public CompareingInstrction(ExecutionElement anOperand){this.localElement=anOperand;}

	/**
	 * @see ExecutableUnit#performTheAction(CommandExecutionManager)
	 */
	@Override
	public void performTheAction(CommandExecutionManager theManager) {
		
		
		ByteValueHolder accumulator = theManager
										.getTheByteValueHolderActingAsAccumulator();
		
		
		int theResult = accumulator
							.getValue()
						- localElement
							.getVaalue(theManager);
		
		
		theManager
			.getAllTheBitsFlags()
			.setAllFlagValues(theResult);
		
		
		theManager
			.addOneToTheExecutionPositionValue();
	}

}
