package com.mdsws.z80;

/**
 * This class is a subclass of the ByteValueHolder and inherits its methods. 
 * Each bit in the byte is treated as a bit.
 */
public class ByteValueHolderAsBits extends ByteValueHolder {
	private static final int CARRY_BIT_POSITION_IN_BYTE = 0;
	private static final int ZERO_BIT_POSITION = 6;
	private static final int SIGN_BIT_POS = 7;

	public ByteValueHolderAsBits(String aName) {
		super(aName);
	}

	public void setAllFlagValues(int anOperationResult) {
		setTheCarryFlagBit(anOperationResult < 0 || anOperationResult > 255);
		setZeroBit((0x00ff & anOperationResult) == 0);
		setSignFlag((0x00ff & anOperationResult) < 0 || (0x00ff & anOperationResult) > Byte.MAX_VALUE);
	}

	public boolean getTheCarryFlagBit() {
		return getBitValueFromTheByte(CARRY_BIT_POSITION_IN_BYTE);
	}

	public void setTheCarryFlagBit(boolean aValue) {
		setBitValue(CARRY_BIT_POSITION_IN_BYTE, aValue);
	}

	public int getTheCarryFlagBitAsIntegerValue() {
		return getTheCarryFlagBit() ? 1 : 0;
	}

	public boolean getZeroBit() {
		return getBitValueFromTheByte(ZERO_BIT_POSITION);
	}

	public void setZeroBit(boolean aValue) {
		setBitValue(ZERO_BIT_POSITION, aValue);
	}

	public boolean getSignFlag() {
		return getBitValueFromTheByte(SIGN_BIT_POS);
	}

	public void setSignFlag(boolean aValue) {
		setBitValue(SIGN_BIT_POS, aValue);
	}

	private boolean getBitValueFromTheByte(int aBit) {
		return ((_Value >> aBit) & 1) == 1;
	}

	private void setBitValue(int aBit, boolean aValue) {
		int theByteMaskToFilter = 1 << aBit; // shifts the one bit
		// apply the beauty mask :-)
		if (aValue)
			this._Value |= theByteMaskToFilter;
		else
			this._Value &= ~theByteMaskToFilter;
	}

}
