package com.mdsws.z80;

/**
 * Objects of this class represent holders of bytes in the system (lioke registers)
 * Each holder can hold only one byte, and none more.
 * It can return the byte by answering the call to its own method of getValue(), which is called from outside of the class.
 */
public class ByteValueHolder {
	public static final String HOLDER_1 = "A"; // the A
	public static final String HOLDER_2 = "B"; // tghe B
	
	public static final String HOLDER_3 = "C"; // the C
	public static final String HOLDER_4 = "D"; // D
	public static final String HOLDER_5 = "E"; // E
	public static final String HOLDER_6 = "F"; // F
	public static final String HOLDER_7 = "H"; // H and L
	public static final String HOLDER_8 = "L";

	/**
	 * its name
	 */
	private String _name;
	
	// its value
	protected byte _Value;
	
	public ByteValueHolder(String aName) {
		this._name = aName;
	}
	
	/**
	 * returns the name
	 * @return the name or null if the name is null
	 */
	public String getItsName() {
		return _name;
	}

	/**
	 * returns the byte
	 * @return the byte value. Returns 0 is the value is 0, etc.
	 */
	public byte getValue() {
		return _Value;
	}

	/**
	 * sets the byte value using the given parameter
	 * @param aValue
	 */
	public void setValue(byte aValue) {
		this._Value = aValue;
	}
}
